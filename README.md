# hello project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

Quarkus é um framework desenvolvido pela Red Hat que possibilita a criação de aplicações Java Cloud Native com uso de Native Image. 
Com Quarkus é possivel criarmos containers que rodem aplicações java sem a necessidade de haver uma JVM, ele utiliza GraalVM, com GraalVM podemos criar um executavel nativo a partir de um codigo Java.
Containers construídos em com Quarkus são até 30% menores, utilizam menos memória RAM e suas aplicaçãoes são inicialiadas em décimos de segundos.

Essa aplicação hello world possui dois endpoints para teste:

/hello
/hello/{nome}

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```
./mvnw quarkus:dev
```

## Packaging and running the application

The application can be packaged using `./mvnw package`.
It produces the `hello-1.0.0-SNAPSHOT-runner.jar` file in the `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

The application is now runnable using `java -jar target/hello-1.0.0-SNAPSHOT-runner.jar`.

## Creating a native executable

You can create a native executable using: `./mvnw package -Pnative`.

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: `./mvnw package -Pnative -Dquarkus.native.container-build=true`.

You can then execute your native executable with: `./target/hello-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/building-native-image.

## Building docker image
docker image build -f src/main/docker/Dockerfile.multistage -t <nome_imagem> .

## Run Container
docker container run -d -p 8081:8080 <nome_imagem>