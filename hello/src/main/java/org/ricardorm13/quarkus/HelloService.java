package org.ricardorm13.quarkus;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class HelloService {

    public String hello(String name) {
        return "Olá " + name;
    }

}